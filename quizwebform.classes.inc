<?php

/**
 * @file
 * Question type, enabling the creation of webform as a questions.
 *
 * The main classes for the quizwebformquestion question type.
 *
 * These inherit or implement code found in quiz_question.classes.inc.
 *
 * Based on:
 * Other question types in the quiz framework.
 */

/**
 * Extension of QuizQuestion.
 */
class QuizwebformQuestion extends QuizQuestion {

  /**
   * Implementation of save.
   *
   * Stores the question in the database.
   *
   * @param bool $is_new
   *   If the node is a new node.
   *
   * @see save()
   *   Save result of quiz.
   */
  public function saveNodeProperties($is_new = FALSE) {
    $is_new = $is_new || $this->node->revision == 1;
    if ($is_new) {
      db_insert('quiz_webform_node_properties')
        ->fields(array(
          'nid' => $this->node->nid,
          'vid' => $this->node->vid,
        ))
        ->execute();
    }
    else {
      // Here Update quiz node properties.
    }

  }

  /**
   * Implementation of validate.
   *
   * QuizQuestion#validate()
   */
  public function validateNode(array &$form) {
    // No validation required.
  }

  /**
   * Implementation of delete.
   *
   * @see delete()
   */
  public function delete($only_this_version = FALSE) {
    if ($only_this_version) {
      db_delete('quiz_webform_node_properties')
        ->condition('question_nid', $this->node->nid)
        ->condition('question_vid', $this->node->vid)
        ->execute();
      db_delete('quiz_webform_user_answers')
        ->condition('nid', $this->node->nid)
        ->condition('vid', $this->node->vid)
        ->execute();
    }
    else {
      db_delete('quiz_webform_node_properties')
        ->condition('nid', $this->node->nid)
        ->execute();
      db_delete('quiz_webform_user_answers')
        ->condition('question_nid', $this->node->nid)
        ->execute();
    }
    parent::delete($only_this_version);
  }

  /**
   * Implementation of getNodeProperties.
   *
   * @see getNodeProperties()
   */
  public function getNodeProperties() {
    if (isset($this->nodeProperties)) {
      return $this->nodeProperties;
    }
    $props = parent::getNodeProperties();

    // Load the properties.
    $res_a = db_query('SELECT * FROM {quiz_webform_node_properties} WHERE nid = :nid AND vid = :vid',
      array(
        ':nid' => $this->node->nid,
        ':vid' => $this->node->vid,
      ))
      ->fetchAssoc();

    if (is_array($res_a)) {
      $props = array_merge($props, $res_a);
    }
    $this->nodeProperties = $props;
    return $props;
  }

  /**
   * Implementation of getNodeView.
   *
   * @see getNodeView()
   */
  public function getNodeView() {
    $content = parent::getNodeView();
    $content['filetypes'] = array(
      '#type' => 'markup',
      '#value' => '<pre> Here File types will show.  </pre>',
    );
    return $content;
  }

  /**
   * Generates the question form.
   *
   * This is called whenever a question is rendered, either.
   * to an administrator or to a quiz taker.
   */
  public function getAnsweringForm(array $form_state = NULL, $rid = NULL) {
    $form = parent::getAnsweringForm($form_state, $rid);
    module_load_include('inc', 'webform', 'includes/webform.components');
    module_load_include('inc', 'webform', 'includes/webform.submissions');
    module_load_include('inc', 'webform', 'components/date');

    $quizwebform_submission = '';
    if ($rid != NULL) {
      $quizwebform_submission = db_query('SELECT qw.quizwebform_submission
        FROM {quiz_webform_user_answers} qw
        WHERE question_nid = :nid AND question_vid = :vid AND result_id = :result_id',
          array(
            ':nid' => $this->node->nid,
            ':vid' => $this->node->vid,
            ':result_id' => $rid,
          ))
         ->fetchField();
    }

    $path = drupal_get_path('module', 'webform');

    drupal_add_css($path . '/css/webform.css');
    drupal_add_js($path . '/js/webform.js');
    $webform = drupal_get_form('webform_client_form_' . $this->node->nid, $this->node);
    $webform = $webform['submitted'];

    $webform = quizwebform_change_managed_file_to_file($webform);
    if ($quizwebform_submission != '') {
      // Here we do auto fill quizwebform on Next or Previous event.
      $webform = quizwebform_get_selected_values($webform, $quizwebform_submission);
    }

    $form['webformquiz'] = $webform;

    return $form;
  }

  /**
   * Implementation of getCreationForm.
   *
   * @see getCreationForm()
   */
  public function getCreationForm(array &$form_state = NULL) {
    // We do not need this.
    return array();
  }

  /**
   * Implementation of getMaximumScore.
   *
   * @see getMaximumScore()
   */
  public function getMaximumScore() {
    return variable_get('quizwebform_default_score', 1);
  }

}

/**
 * Extension of QuizQuestionResponse.
 */
class QuizwebformResponse extends QuizQuestionResponse {
  /**
   * ID of the answer.
   */
  protected $answerId = 0;

  /**
   * Quiz question response constructor.
   */
  public function __construct($result_id, stdClass $question_node, $tries = NULL) {
    parent::__construct($result_id, $question_node, $tries);

    $tries = $_FILES;
    $this->answer = $tries;

    $result = new stdClass();
    $result->is_correct = TRUE;
    $this->question->score_weight = 0;
    $this->evaluated = TRUE;
    $this->result_id = $result_id;

    // Question has been taken. We get the answer data from the database.
    $r = db_query('SELECT * FROM {quiz_webform_user_answers}
    WHERE question_nid = :question_nid AND question_vid = :question_vid AND result_id = :result_id',
    array(
      ':question_nid' => $question_node->nid,
      ':question_vid' => $question_node->vid,
      ':result_id' => $result_id,
    ))->fetchAssoc();

    if (is_array($r)) {
      $this->score = $r['score'];
      $this->answerId = $r['answer_id'];
    }
    else {
      $this->score = variable_get('quizwebform_default_score', 1);
    }
  }

  /**
   * Implementation of isValid.
   *
   * @see QuizQuestionResponse#isValid()
   */
  public function isValid() {
    module_load_include('inc', 'webform', 'includes/webform.components');
    module_load_include('inc', 'webform', 'includes/webform.submissions');
    module_load_include('inc', 'webform', 'components/date');

    $quizwebform_row = db_query('SELECT qw.question_nid, qw.quizwebform_submission
    FROM {quiz_webform_user_answers} qw
    WHERE question_nid = :nid AND question_vid = :vid AND result_id = :result_id',
    array(
      ':nid' => $this->question->nid,
      ':vid' => $this->question->vid,
      ':result_id' => $this->rid,
    ))->fetchAssoc();

    $quizwebform_nid = isset($quizwebform_row['question_nid']) ? $quizwebform_row['question_nid'] : 0;
    $quizwebform_submission = isset($quizwebform_row['quizwebform_submission']) ? $quizwebform_row['quizwebform_submission'] : '';
    $file = '';
    foreach ($_FILES as $field_name => $val) {
      if ($val['name'] != '') {
        $image = file_get_contents($_FILES[$field_name]['tmp_name']);
        // Create the directory if it does not already exist,
        // otherwise check the permissions.
        $directory = 'public://webformquiz';
        file_prepare_directory($directory, FILE_CREATE_DIRECTORY);

        // Saves a file to the specified destination,
        // and creates a database entry.
        $file = file_save_data($image, $directory . '/' . $_FILES[$field_name]['name'], FILE_EXISTS_RENAME);
        // Set the file status to permanent.
        $file->status = FILE_STATUS_PERMANENT;
        file_save($file);
        $_POST['submitted'][$field_name] = $file->fid;
      }
      else {
        if ($quizwebform_submission != '') {
          $submission_arr = unserialize($quizwebform_submission);
          $_POST['submitted'][$field_name] = $submission_arr['submitted'][$field_name];
        }
      }
    }

    if (!$quizwebform_nid) {
      $this->save();
    }
    else {
      db_update('quiz_webform_user_answers')->fields(array(
        'quizwebform_submission' => serialize($_POST),
        ))
        ->condition('question_nid', $this->question->nid)
        ->condition('question_vid', $this->question->vid)
        ->condition('result_id', $this->rid)
        ->execute();
    }

    return TRUE;
  }

  /**
   * Implementation of save.
   *
   * @see QuizQuestionResponse#save()
   */
  public function save() {
    $this->answerId = db_insert('quiz_webform_user_answers')
      ->fields(array(
        'result_id' => $this->rid,
        'question_vid' => $this->question->vid,
        'question_nid' => $this->question->nid,
        'quizwebform_submission' => serialize($_POST),
        'score' => $this->score(),
      ))
      ->execute();
  }

  /**
   * Implementation of delete.
   *
   * @see QuizQuestionResponse#delete()
   */
  public function delete() {
    db_delete('quiz_webform_user_answers')
      ->condition('question_nid', $this->question->nid)
      ->condition('question_vid', $this->question->vid)
      ->condition('result_id', $this->rid)
      ->execute();
  }

  /**
   * Implementation of score.
   *
   * @return int
   *   uint.
   *
   * @see QuizQuestionResponse#score()
   */
  public function score() {
    return variable_get('quizwebform_default_score', 0);
  }

  /**
   * Implementation of getResponse.
   *
   * @return int
   *   answer.
   *
   * @see QuizQuestionResponse#getResponse()
   */
  public function getResponse() {
    return $this->answer;
  }

  /**
   * Implimenting report form.
   *
   * @see getReportFormResponse()
   */
  public function getReportFormResponse($showpoints = TRUE, $showfeedback = TRUE, $allow_scoring = FALSE) {
    global $user;

    module_load_include('inc', 'webform', 'webform.module');
    module_load_include('inc', 'webform', 'includes/webform.submissions');

    $result_id = $this->question->answers[0]['result_id'];
    $nid = $this->question->nid;
    $vid = $this->question->vid;
    $node = node_load($nid);

    $result = db_query('SELECT *
      FROM {quiz_webform_user_answers}
      WHERE result_id = :result_id AND question_nid = :question_nid AND question_vid = :question_vid',
      array(
        ':result_id' => $result_id,
        ':question_nid' => $this->question->nid,
        ':question_vid' => $this->question->vid,
      ))
        ->fetchAssoc();

    if (!$result['quizwebform_sid']) {
      $sdata = unserialize($result['quizwebform_submission']);

      $fdata = _webform_client_form_submit_flatten($node, $sdata['submitted']);
      // Hack for date and time fields.
      foreach ($fdata as $key => $value) {
        if (is_array($value) && array_key_exists('month', $value)) {
          $fdata[$key] = webform_date_string($value, 'date');
        }

        if (is_array($value) && array_key_exists('hour', $value)) {
          $fdata[$key] = webform_date_string($value, 'time');
        }
      }

      $data = webform_submission_data($node, $fdata);

      $submission = (object) array(
        'nid' => $nid,
        'uid' => $user->uid,
        'sid' => NULL,
        'submitted' => REQUEST_TIME,
        'completed' => REQUEST_TIME,
        'remote_addr' => ip_address(),
        'is_draft' => FALSE,
        'data' => $data,
      );

      $sid = webform_submission_insert($node, $submission);
      quizwebform_update_sid($sid, $result_id, $nid, $vid);
    }
    else {
      $sid = $result['quizwebform_sid'];
    }

    $markup = quizwebform_submission_markup($sid, $node);
    return array(
      '#type' => 'markup',
      '#markup' => $markup,
    );
  }

}

/**
 * Getting submission result markup for webform.
 */
function quizwebform_submission_markup($sid, $node) {
  $submission = webform_get_submission($node->nid, $sid);
  $markup = webform_submission_render($node, $submission, '', 'html');
  $markup = drupal_render($markup);
  return $markup;
}

/**
 * Setting default value of webform components.
 */
function quizwebform_get_selected_values($webform, $submission = '', $submitted_vals = array()) {
  if ($submission == '' && !count($submitted_vals)) {
    return;
  }

  $submission_arr = unserialize($submission);
  if (count($submitted_vals)) {
    $submitted_values = $submitted_vals;
  }
  else {
    $submission_arr = unserialize($submission);
    $submitted_values = $submission_arr['submitted'];
  }

  foreach ($webform as $name => $component) {
    if (isset($component['#webform_component'])) {
      switch ($component['#webform_component']['type']) {
        case "textfield":
        case "email":
        case "number":
        case "textarea":
          if (isset($submitted_values[$name])) {
            $webform[$name]['#value'] = $submitted_values[$name];
          }
          break;

        case "select":
          if ($component['#type'] == 'checkboxes') {
            foreach ($component as $key => $chkbox) {
              if (isset($chkbox['#type']) && isset($submitted_values[$name][$key])) {
                $webform[$name][$key]['#value'] = $key;
              }
            }
          }

          if ($component['#type'] == 'radios') {
            $form_key = $component['#webform_component']['form_key'];
            foreach ($component as $key => $chkbox) {
              if (isset($chkbox['#type']) && isset($submitted_values[$form_key]) && $submitted_values[$form_key] == $key) {
                $webform[$name][$key]['#value'] = $key;
              }
            }
          }

          if ($component['#type'] == 'select') {
            $form_key = $component['#webform_component']['form_key'];
            if (isset($chkbox['#type']) && isset($submitted_values[$form_key])) {
              $webform[$name]['#value'] = $submitted_values[$form_key];
            }
          }

          break;

        case "date":
          $form_key = $component['#webform_component']['form_key'];
          $webform[$name]['#default_value'] = $submitted_values[$name];
          $webform[$name]['#default_value']['value'] = $component['month']['#options'][$submitted_values[$name]['month']] . ' ' . $submitted_values[$name]['day'] . ' ' . $submitted_values[$name]['year'];
          if (isset($submitted_values[$form_key])) {
            $webform[$name]['#value'] = $submitted_values[$form_key];
            foreach ($component as $key => $selectbox) {
              if (isset($selectbox['#type']) && isset($submitted_values[$name][$key])) {
                $webform[$name][$key]['#value'] = $submitted_values[$name][$key];
              }
            }
          }
          break;

        case "time":
          $form_key = $component['#webform_component']['form_key'];
          $webform[$name]['#default_value'] = $submitted_values[$name];
          $webform[$name]['#webform_component']['value'] = $submitted_values[$name]['hour'] . ':' . $submitted_values[$name]['minute'] . $submitted_values[$name]['ampm'];
          if (isset($submitted_values[$form_key])) {
            $webform[$name]['#value'] = $submitted_values[$form_key];
            $webform[$name]['#default_value'] = $submitted_values[$form_key];
            foreach ($component as $key => $selectbox) {
              if (isset($selectbox['#type']) && isset($submitted_values[$name][$key])) {
                $webform[$name][$key]['#default_value'] = $submitted_values[$name][$key];
                $webform[$name][$key]['#value'] = $submitted_values[$name][$key];
                if ($key == 'ampm') {
                  $ampm = $submitted_values[$name][$key];
                  $webform[$name][$key][$ampm]['#value'] = $ampm;
                  $webform[$name][$key][$ampm]['#default_value'] = $ampm;
                  $ampm == 'am' ? $webform[$name][$key]['pm']['#value'] = '' : $webform[$name][$key]['pm']['#value'] = '';
                }
              }
            }
          }
          break;

        case "grid":
          $form_key = $component['#webform_component']['form_key'];
          $webform[$name]['#default_value'] = $submitted_values[$form_key];
          foreach ($component['#grid_questions'] as $key => $val) {
            $webform[$name][$key]['#defalt_value'] = $submitted_values[$form_key][$key];
            if (isset($submitted_values[$form_key][$key])) {
              foreach ($component[$key] as $k => $v) {
                if (isset($v['#type'])) {
                  $webform[$name][$key][$k]['#default_value'] = $submitted_values[$form_key][$key];
                }
              }
            }
          }
          break;

        case "file":
          if (isset($submitted_values[$name])) {
            $file_markup = quizwebform_get_file_markup($submitted_values[$name]);
            $webform[$name]['#suffix'] = '<div class="form-item webform-component">' . $file_markup . '</div>';
          }
          break;

        case "fieldset":
          $webform[$name] = quizwebform_get_selected_values($webform[$name], '', $submitted_values[$name]);
          break;
      }
    }
  }

  return $webform;
}

/**
 * Changeing managed file to file.
 */
function quizwebform_change_managed_file_to_file($webform) {
  foreach ($webform as $key => $val) {
    if (isset($webform[$key]['#type']) && $webform[$key]['#type'] == 'managed_file') {
      $webform[$key] = array(
        '#type' => 'file',
        '#name' => $key,
        '#weight' => $webform[$key]['#weight'],
        '#webform_component' => $webform[$key]['#webform_component'],
        '#description' => $webform[$key]['#description'],
      );
    }

    if (isset($webform[$key]['#type']) && $webform[$key]['#type'] == 'fieldset') {
      $webform[$key] = quizwebform_change_managed_file_to_file($webform[$key]);
    }
  }
  return $webform;
}

/**
 * Getting uploaded file link.
 */
function quizwebform_get_file_markup($fid = '') {
  if (!$fid) {
    return FALSE;
  }

  $file = file_load($fid);
  if (!$file) {
    return FALSE;
  }

  $file_link = t('File:') . l($file->filename, file_create_url($file->uri), array('attributes' => array('target' => '_blank')));
  return $file_link;
}
