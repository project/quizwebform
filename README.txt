CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation

INTRODUCTION
------------

The Quiz Webform modules allows you to create interactive question with webform
for your visitors.

REQUIREMENTS
------------

This module requires the following modules:

 * Quiz (https://www.drupal.org/project/quiz)
 * Quiz Question (https://www.drupal.org/project/quiz)
 * Webform (https://www.drupal.org/project/webform)

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.
